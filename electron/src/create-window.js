import path from "path";
import { fileURLToPath } from "url";
import { app, BrowserWindow } from "electron";

const partition = String(+new Date())
const mainWindowServer = (win, hash = "") => {
  // const rootPath = app.getAppPath();
  // const entryPath = path.join(rootPath,"src", "index.html");
  // win.loadFile(entryPath);
  win.loadURL('http://localhost:5173')
};
const __dirname = path.dirname(fileURLToPath(import.meta.url));
export const createMainWindow = () => {
  const mainWinOtp = { // 创建主应用窗口
    width: 1024,
    height: 600,
    show: true,
    devTools: true,
    webPreferences: {
      partition,
      webSecurity: false, // 禁用同源策略，允许跨域
      preload: path.join(__dirname, "preload.js"), // 注入预加载文件（主进程和渲染进程沟通的桥梁[利用ipc]）
    },
  };
  const mainWin = new BrowserWindow(mainWinOtp);
  mainWindowServer(mainWin); // 启动窗口加载页面功能（用户才能看到electron窗口中的内容）
  // mainWin.webContents.openDevTools({
  //   mode: "detach",
  // });
  return mainWin;
};