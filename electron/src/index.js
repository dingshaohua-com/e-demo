import os from 'os';
import { app, BrowserWindow } from "electron";
import { createMainWindow } from "./create-window.js";

// https://github.com/electron/electron/issues/43415
if (os.platform() === 'darwin' && os.arch() === 'x64') {
  app.disableHardwareAcceleration();
}

app.commandLine.appendSwitch('lang', 'zh-CN'); // 设置为中文
app.whenReady().then(() => {
  createMainWindow();
  app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createMainWindow();
    }
  });
});

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") app.quit();
});